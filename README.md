# Raychat Test Project

The conditions that are met:

> - React with manual webpack configs
> - Redux as a state manager
> - Sass and a fully manual styling
> - Axios as a http fetcher
> - Eva Icons as an icon pack

The Functionalities:

> - Fetch GitHub user info
> - Switching between Dark and Light mode
> - Saving the chosen theme after reloading the app
> - Print error if the user does not exist
> - Showing the loader while it is fetching data

---

## Demo

![demo](./demo.gif)

---

## Start Dev Server

```bash
yarn dev
```

---

## Build and Run Production

```bash
yarn build
```

First install `serve` module for running a server after the build process is done:

```bash
npm i -g serve
```

And for running the server on `/build` foloer:

```bash
serve -s build
```

Open up the localhost on port **5000** (default)
