const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const prod = "production";

module.exports = (env) => ({
  entry: "./src/index.js",
  output: {
    filename: "bundle.js",
    path: path.join(__dirname, "./build")
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.(s*)css$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
      { // config for images
        test: /\.(png|jpg|jpeg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'images',
            }
          }
        ],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      { // config for fonts
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'fonts',
            }
          }
        ],
      }
    ]
  },
  resolve: {
    extensions: [".webpack-loader.js", ".web-loader.js", ".loader.js", ".js", ".jsx"],
    modules: [
      path.resolve(__dirname, "node_modules"),
      path.join(__dirname, "./src")
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: path.resolve(__dirname, "./public", "index.html"),
      filename: "./index.html"
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, "./build"),
    port: "3000"
  }
});