import React from 'react'
import PropTypes from 'prop-types';
import Sun from '../../assets/icons/fill/svg/sun.svg';
import Moon from '../../assets/icons/fill/svg/moon.svg';
import './Header.scss';

export default function Header({ theme, onToggleTheme }) {
  const themeClass = theme ? "dark-theme" : "";
  return (
    <div className="header">
      <h1 className={`title ${themeClass}`}>GitHub Profiles</h1>
      <div className={`mode ${themeClass}`} onClick={() => onToggleTheme()} >
        {
          theme &&
          <Sun />
        }
        {
          !theme &&
          <Moon />
        }
      </div>
    </div>
  )
};

Header.propTypes = {
  theme: PropTypes.bool.isRequired,
  onToggleTheme: PropTypes.func.isRequired,
};
