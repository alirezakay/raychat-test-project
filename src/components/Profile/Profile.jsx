import React from 'react';
import PropTypes from 'prop-types';
import Copy from '../../assets/icons/fill/svg/copy.svg';
import Star from '../../assets/icons/fill/svg/star.svg';
import Code from '../../assets/icons/fill/svg/code.svg';
import './Profile.scss';

export default function Profile({ data, theme }) {
  const themeClass = theme ? "dark-theme" : "";

  return (
    <div className="profile">
      {
        data === 404 &&
        <div className="error">User not found :(</div>
      }
      {
        (data.name || data.avatar_url) &&
        <div className="wrapper">
          <div className="info">
            <div className="avatar"><img src={data.avatar_url} alt={data.name} /></div>
            <div className={`name ${themeClass}`}>{data.name}</div>
            <div className={`company ${themeClass}`}><span>Company</span>: {data.company}</div>
            <div className={`location ${themeClass}`}><span>Location</span>: {data.location}</div>
            <div className={`website ${themeClass}`}><span>Website</span>: <a href={data.website} target="_blank" rel="noopener noreferrer">{String(data.website).replace(/http(s?):\/\/|www./g, "")}</a></div>
          </div>
          <div className="repos">
            {
              (data.repos || []).map((repo) => {
                const { name, description, forks_count, stargazers_count, language, html_url } = repo;
                return (
                  <div key={repo.name} className="repo">
                    <div className={`card ${themeClass}`}>
                      <div className={`name ${themeClass}`}><a href={html_url} target="_blank" rel="noopener noreferrer">{name}</a></div>
                      <div className="desc">{description}</div>
                      <div className={`bottom ${themeClass}`}>
                        <div className="lang"><Code />&nbsp; {language}</div>
                        <div className="forks"><Copy />&nbsp; {forks_count}</div>
                        <div className="stars"><Star />&nbsp; {stargazers_count}</div>
                      </div>
                    </div>
                  </div>
                )
              })
            }
          </div>
        </div>
      }
    </div>
  );
}

Profile.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    avatar_url: PropTypes.string,
    company: PropTypes.string,
    website: PropTypes.string,
    location: PropTypes.string,
    repos: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired,
      description: PropTypes.string,
      stargazers_count: PropTypes.number.isRequired,
      forks_count: PropTypes.number.isRequired,
      language: PropTypes.string.isRequired,
      html_url: PropTypes.string.isRequired,
    })),
  }),
  theme: PropTypes.bool.isRequired,
};
