import React from 'react';
import PropTypes from 'prop-types';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import './Profile.scss';

const dark = "#292929";
const light = "#fafafa";
const darkHighlightColor = "#252525";
const lightHighlightColor = "#f5f5f5";

export default function ProfilePlaceHolder({ theme }) {
  return (
    <SkeletonTheme color={theme ? dark : light} highlightColor={theme ? darkHighlightColor : lightHighlightColor}>
      <div className="profile placeholder">
        <div className="wrapper">
          <div className="info">
            <div className="avatar"><Skeleton /></div>
            <div className="name"><Skeleton /></div>
            <div className="company"><Skeleton /></div>
            <div className="location"><Skeleton /></div>
            <div className="website"><Skeleton /></div>
          </div>
          <div className="repos">
            {
              ([1, 2, 3, 4]).map((repo) => {
                return (
                  <div key={repo} className="repo">
                    <div className="card">
                      <Skeleton />
                    </div>
                  </div>
                )
              })
            }
          </div>
        </div>
      </div>
    </SkeletonTheme>
  );
}

ProfilePlaceHolder.propTypes = {
  theme: PropTypes.bool.isRequired,
};
