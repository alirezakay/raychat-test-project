import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import { onToggleTheme } from './actions';
import './App.scss';
import { useCookies } from 'react-cookie';
import Index from './pages/index';
import Header from './components/Header/Header';

function App({ theme, onToggleTheme }) {
  const [cookies, setCookie] = useCookies(['theme']);

  useEffect(() => {
    if(cookies.theme!=undefined || cookies.theme!=null){
      if(String(cookies.theme) !== String(theme)){
        onToggleTheme(setCookie);
      }
    }
  }, [])

  const themeClass = theme ? "dark-theme" : "";
  return (
    <div id="App" className={themeClass}>
      <div className="container">
        <header id="header">
          <Header theme={theme} onToggleTheme={() => onToggleTheme(setCookie)} />
        </header>
        <main id="main">
          <Index theme={theme} />
        </main>
        <footer id="footer" />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    theme: state.input.theme,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onToggleTheme: (setCookie) => dispatch(onToggleTheme(setCookie)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
