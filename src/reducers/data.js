const initState = {
  profileInfo: {}
}

export default (state=initState, action) => {
  const newState = {...state};
  switch(action.type){
    case "SET_PROFILE_INFO":
      newState.profileInfo = action.data;
      break;
  }
  return newState;
}