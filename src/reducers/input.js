/**
 * theme: false => light
 * theme: true => dark
 */
const initState = {
  username: "",
  theme: false,
}

export default (state = initState, action) => {
  const newState = { ...state };
  switch (action.type) {
    case "CHANGE_USERNAME":
      newState.username = String(action.username).trim();
      break;
    case "TOGGLE_THEME":
      newState.theme = !state.theme;
      action.setCookie('theme', newState.theme, { path: '/' });
      break;
  }
  
  return newState;
}