const initState = {
  loadingProfile: false,
}

export default (state = initState, action) => {
  const newState = { ...state };
  switch (action.type) {
    case "SET_LOADING_PROFILE":
      newState.loadingProfile = action.loading;
      break;
  }
  return newState;
}