import { combineReducers } from 'redux';
import data from './data';
import input from './input';
import loading from './loading';

export default combineReducers({
  data,
  input,
  loading,
});
