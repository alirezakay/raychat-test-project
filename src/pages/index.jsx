import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import axios from 'axios';
import { setProfileInfo, onChangeUsername, setLoadingProfile } from '../actions';
import Profile from '../components/Profile/Profile';
import ProfilePlaceHolder from '../components/Profile/ProfilePlaceHolder';
import { MoonLoader } from "react-spinners";
import Search from '../assets/icons/fill/svg/search.svg';
import '../assets/styles/index.scss';

const fetchData = (username, setProfileInfo, setLoadingProfile) => {
  if (!username) {
    setProfileInfo({});
    return;
  }
  setLoadingProfile(true);
  username = username[0] === "@" ? username.replace("@", "") : username;
  axios.get(`https://api.github.com/users/${username}`)
    .then((res) => {
      const { avatar_url, company, name, blog: website, location } = res.data;
      const info = {
        avatar_url,
        company,
        name,
        website,
        location,
      }
      axios.get(`https://api.github.com/users/${username}/repos?type=owner&sort=updated`)
        .then((repos) => {
          info.repos = (repos.data || []).splice(0, 4).map((repo) => {
            const { name, description, forks_count, stargazers_count, language, html_url } = repo;
            return {
              name,
              description,
              forks_count,
              stargazers_count,
              language,
              html_url,
            };
          });
          setLoadingProfile(false);
          setProfileInfo(info)
        })
    })
    .catch((err) => {
      console.error(err);
      setLoadingProfile(false);
      setProfileInfo(404)
    })
}

function Index(props) {
  const { username, profileInfo, setProfileInfo, onChangeUsername, loadingProfile, setLoadingProfile, theme } = props;
  const themeClass = theme ? "dark-theme" : "";

  return (
    <div id="Index">
      <section className="top">
        <div className="desc">Enter a GitHub username, to see the magic.</div>
        <div className="fields">
          <label className="label">GitHub username:</label>
          <div className="group-input">
            <input
              type="text"
              placeholder="@username"
              className={`input ${themeClass}`}
              value={username}
              onChange={(e) => onChangeUsername(e.target.value)}
              onKeyDown={(e) => e.key === "Enter" ? fetchData(username, setProfileInfo, setLoadingProfile) : () => { }}
            />
            <button
              className={`search ${themeClass}`}
              onClick={() => fetchData(username, setProfileInfo, setLoadingProfile)}
            >
              {
                loadingProfile &&
                <MoonLoader
                  css="margin: 0 auto;"
                  size={15}
                  color={"#123abc"}
                />
              }
              {
                !loadingProfile &&
                <Search />
              }
            </button>
          </div>
        </div>
      </section>
      <section className="results">
        {
          loadingProfile &&
          <ProfilePlaceHolder theme={theme} />
        }
        {
          !loadingProfile &&
          <Profile data={profileInfo} theme={theme} />
        }
      </section>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    profileInfo: state.data.profileInfo,
    username: state.input.username,
    loadingProfile: state.loading.loadingProfile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setProfileInfo: (data) => dispatch(setProfileInfo(data)),
    onChangeUsername: (username) => dispatch(onChangeUsername(username)),
    setLoadingProfile: (loading) => dispatch(setLoadingProfile(loading)),
  };
};

Index.propTypes = {
  theme: PropTypes.bool.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Index);
