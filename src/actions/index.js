export const onToggleTheme = (setCookie) => ({type: "TOGGLE_THEME", setCookie})
export const onChangeUsername = (username) => ({type: "CHANGE_USERNAME", username})
export const setProfileInfo = (data) => ({type: "SET_PROFILE_INFO", data});
export const setLoadingProfile = (loading) => ({type: "SET_LOADING_PROFILE", loading});